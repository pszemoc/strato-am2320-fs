#include "am2320_fs.hpp"
#include <fstream>

am2320_fs_t::am2320_fs_t() {
  std::ofstream f(am2320_driver::new_device);
  f << "am2320 0x5c";
  f.close();
}

am2320_fs_t::~am2320_fs_t() {
  std::ofstream f(am2320_driver::delete_device);
  f << "0x5c";
  f.close();
}

float am2320_fs_t::read_temperature() {
  std::string val;
  std::ifstream f(am2320_driver::temptemperature);
  f >> val;
  f.close();
  return static_cast<float>(std::stoi(val)) / 10.0;
}

float am2320_fs_t::read_humidity() {
  std::string val;
  std::ifstream f(am2320_driver::humidity);
  f >> val;
  f.close();
  return static_cast<float>(std::stoi(val)) / 10.0;
}