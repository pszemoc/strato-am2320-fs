#include "am2320_fs.hpp"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "strato-frames/interior_env_frame.hpp"
#include <memory>
#include <zmq.hpp>
#include <libconfig.h++>
#include <csignal>
#include <stdexcept>

class Application {
private:
  zmq::context_t                    context;
  std::shared_ptr<zmq::socket_t>    socket;
  std::shared_ptr<am2320_fs_t>      am2320;

  inline static bool running = false;
   static void termination_handler(int) {
    spdlog::info("Termination requested");
    running = false;
  }

  void init_log() {
    auto file_logger = spdlog::basic_logger_mt("am2320", "/var/log/balloon/am2320.log");
    spdlog::set_default_logger(file_logger);
    spdlog::default_logger()->flush_on(spdlog::level::level_enum::info);
  }

  void init_signals() {
    std::signal(SIGTERM, Application::termination_handler);
  }

  void init_socket() {
    libconfig::Config cfg;
    try {
      cfg.readFile("/etc/sp-config/sp.cfg");
    } catch (const libconfig::FileIOException &fioex) {
      throw std::runtime_error("I/O error while reading file.");
    } catch (const libconfig::ParseException &pex) {
      std::stringstream ss;
      ss << "Parse error at " << pex.getFile() << ":" << pex.getLine()
            << " - " << pex.getError() << std::endl;
      throw std::runtime_error(ss.str().c_str());
    }

    const libconfig::Setting& root = cfg.getRoot();
    int port;
    if (!root["am2320"].lookupValue("am2320_port", port)) {
      throw std::runtime_error("am2320 port not defined in config.");
    }
    spdlog::info("Config parsed.");

    socket = std::make_shared<zmq::socket_t>(context, ZMQ_PUB);
    if (socket != nullptr) {
      socket->bind("tcp://*:" + std::to_string(port));
    } else {
      throw std::runtime_error("Error while creating socket.");
    }
    spdlog::info("Socket opened at: {}", port);
  }

public:
  Application() : context(1) {}    

  int exec() {
    running = true;
    init_log();
    init_signals();
    try {
      init_socket();
      am2320 = std::make_shared<am2320_fs_t>();
    } catch (const std::exception& e) {
      spdlog::error(e.what());
      running = false;
      return -1;
    }

    while (running) {
      std::this_thread::sleep_for(std::chrono::seconds(1)); 

      try {
        interior_env_frame_t frame(
          am2320->read_temperature(),
          am2320->read_humidity()
        );
        spdlog::info("Interior temperature: {} deg C, Humidity: {} %RH", frame.temperature, frame.humidity);
        socket->send(frame_topic::interior_env.begin(), frame_topic::interior_env.end(), ZMQ_SNDMORE);
        socket->send(zmq::const_buffer(&frame, sizeof(interior_env_frame_t)));
      } catch (const std::exception& e) {
        spdlog::error(e.what());
      }
    }
    return 0;
  }
};
