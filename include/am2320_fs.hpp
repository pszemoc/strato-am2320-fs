#pragma once

namespace am2320_driver {
  constexpr char new_device[]         = "/sys/class/i2c-adapter/i2c-1/new_device";
  constexpr char delete_device[]      = "/sys/class/i2c-adapter/i2c-1/delete_device";
  constexpr char temptemperature[]    = "/sys/bus/i2c/devices/1-005c/temp1_input";
  constexpr char humidity[]           = "/sys/bus/i2c/devices/1-005c/humidity1_input";
}

class am2320_fs_t {
public:
  am2320_fs_t();
  ~am2320_fs_t();

  float read_temperature();  // [deg C]
  float read_humidity();     // [%RH] 
};
